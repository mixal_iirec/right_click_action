SOURCES = $(patsubst ./%,%,$(shell find ./ -name "*.cpp" | grep -v 'test.cpp'))
SOURCES_C = $(patsubst ./%,%,$(shell find ./ -name "*.c" | grep -v 'test.c'))

OBJS = $(patsubst %.cpp,o/%.o,$(SOURCES))
DEPS = $(patsubst %.cpp,d/%.d,$(SOURCES))

OBJS_C = $(patsubst %.c,o/%.o,$(SOURCES_C))
DEPS_C = $(patsubst %.c,d/%.d,$(SOURCES_C))


INCLUDES = "$(shell cat '.makefile_set/modules' '.makefile_set/libraries')"

FLAGS_C = $(shell cat .makefile_set/flags_c .makefile_set/flags_both) $(addprefix -I, $(INCLUDES)) -g
CXXFLAGS = $(shell cat .makefile_set/flags_c++ .makefile_set/flags_both) $(addprefix -I, $(INCLUDES)) -g
CXXFLAGSOUT = $(shell cat .makefile_set/libraries_comp)

all: $(OBJS) $(OBJS_C)
	$(CXX) -o run $^ $(CXXFLAGSOUT)

d/%.d: %.cpp
	$(CXX) $(CXXFLAGS) -MM -MT '$(patsubst %.cpp,o/%.o,$<)' $< -MF $@
	echo "	$(CXX) $(CXXFLAGS) -o $(patsubst %.cpp,o/%.o,$<) -c $<" >> $@

d/%.d: %.c
	$(CC) $(FLAGS_C) -MM -MT '$(patsubst %.c,o/%.o,$<)' $< -MF $@
	echo "	$(CC) $(FLAGS_C) -o $(patsubst %.c,o/%.o,$<) -c $<" >> $@

include $(DEPS) $(DEPS_C)

clean:
	find ./ -name "*.d" -delete
	find ./ -name "*.o" -delete
	rm run

#to update dependences if changed in header
update:
	find ./ -name "*.d" -delete

.PRECIOUS: $(DEPS) $(DEPS_C)
