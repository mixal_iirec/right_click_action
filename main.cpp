#include "modules.h"
#include <stdio.h>
#include <cstdlib>
#include <chrono>
#include <iostream>

using namespace std;

int main(int argc, const char* argv[]){
	int time_diff_millis = 300;
	const char* commandr = argc > 1 ? argv[1] : "";
	const char* commandm = argc > 2 ? argv[2] : "";


	using clock = chrono::high_resolution_clock;

	clock::time_point lastr = clock::now();
	clock::time_point lastm = clock::now();

	bool ll = 0;
	bool lm = 0;
	bool lr = 0;

	float change_x = 0;
	float change_y = 0;
	int scrolled = 0;
	for(;;){
		char f = getc(stdin);
		char x = getc(stdin);
		char y = getc(stdin);

		bool l = f & 0x1;
		bool r = f & 0x2;
		bool m = f & 0x4;

		if(l != ll){
			if(l){//pressed
			}else{//released
			}
			ll = l;
		}
		if(m != lm){
			if(m){//pressed
				auto now = clock::now();
				auto diff = chrono::duration_cast<chrono::milliseconds>(now - lastm);
				if(diff.count() < time_diff_millis){
					system(commandm);
				}
				lastm = now;
			}else{//released
			}
			lm = m;
		}
		if(r != lr){
			if(r){//pressed
				auto now = clock::now();
				auto diff = chrono::duration_cast<chrono::milliseconds>(now - lastr);
				if(diff.count() < time_diff_millis){
					system(commandr);
				}
				lastr = now;
			}else{//released
			}
			lr = r;
			change_x = 0;
			change_y = 0;
			scrolled = 0;
		}
		if(r){
			change_x += x;
			change_y += y;
			const int c = 8;

			auto alt = [&](){
				if(scrolled < 2){
					scrolled++;
					system("xdotool key Alt"); //closes qutebrowser right click menu
				}
			};

			/*if(change_x > c){
				alt();
				change_x -= c;
				system("xdotool key Right");
			}else if(change_x < -c){
				alt();
				change_x += c;
				system("xdotool key Left");
			}*/
			float my = change_y / c;
			if(my > 1){
				alt();
				change_y -= int(my) * c;
				//string cmd = "xdotool key --delay 1 --repeat " + to_string(int(my)) + " Up";
				string cmd = "xdotool click --delay 1 --repeat " + to_string(int(my)) + " 4";
				system(cmd.c_str());
			}else if(my < -1){
				alt();
				change_y += int(-my)*c;
				//string cmd = "xdotool key --delay 1 --repeat " + to_string(int(-my)) + " Down";
				string cmd = "xdotool click --delay 1 --repeat " + to_string(int(-my)) + " 5";
				system(cmd.c_str());
			}
		}
	}

	return 0;
}
